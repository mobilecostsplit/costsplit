package com.costsplit.mobile.model.uielements;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Timo on 09.04.15.
 */
public class UICostItem {
//    public static ArrayList<UICostItem> items = new ArrayList<>();

    public static void addElement(String title, double amount, String payedByName, String payedForNames){
        UICostItem item = new UICostItem();
        item.title = title;
        item.amount = amount;
        item.payedByName = payedByName;
        item.payedForNames = payedForNames;
//        items.add(item);
    }

    private String title;
    private double amount;
    private String payedByName;
    private String payedForNames;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = Double.valueOf(new DecimalFormat("#.##").format(amount));
    }

    public String getPayedByName() {
        return payedByName;
    }

    public void setPayedByName(String payedByName) {
        this.payedByName = payedByName;
    }

    public String getPayedForNames() {
        return payedForNames;
    }

    public void setPayedForNames(String payedForNames) {
        this.payedForNames = payedForNames;
    }
}
