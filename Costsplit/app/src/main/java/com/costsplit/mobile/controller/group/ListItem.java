package com.costsplit.mobile.controller.group;

/**
 * Created by D056691 on 31.03.2015.
 */
public class ListItem {
    public String GroupName;
    public String GroupMemberNumber;
    public long GroupId;
    public double totalExp;

    // default constructor
    public ListItem() {
        this("GroupName","GroupMemberNumber", 123,1234);
    }

    // main constructor
    public ListItem(String GroupName, String GroupMemberNumber, double totalExp,  long GroupId) {
        super();
        this.GroupName = GroupName;
        this.GroupMemberNumber = GroupMemberNumber;
        this.GroupId = GroupId;
        this.totalExp = totalExp;
    }

    public String toString() {
        return this.GroupId+"";
    }
}
