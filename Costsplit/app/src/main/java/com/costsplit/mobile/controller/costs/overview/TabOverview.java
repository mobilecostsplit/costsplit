package com.costsplit.mobile.controller.costs.overview;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.costsplit.mobile.costsplit.R;
import com.costsplit.mobile.dbaccess.ServicesCostsInterface;
import com.costsplit.mobile.model.CostItem;
import com.costsplit.mobile.model.Group;
import com.costsplit.mobile.model.User;
import com.costsplit.mobile.model.uielements.UICostOverview;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Created by Timo on 31.03.15.
 */
public class TabOverview extends Fragment implements ServicesCostsInterface, SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView mRecyclerView;
    private OverviewAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SwipeRefreshLayout swipeLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_overview,container,false);

        mRecyclerView = (RecyclerView)v.findViewById(R.id.CostsOverviewRecyclerView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        //create ArrayList<UICostOverview> with header for the total expenses per member
        HashMap<Long, UICostOverview> map = this.calculateTotalExpensesPerMember(CostsActivity.groupid);
        ArrayList<UICostOverview> listAmount = new ArrayList<>();
        listAmount.add(new UICostOverview("Who payed how much?", -1, OverviewAdapter.TYPE_HEADER));
        listAmount.addAll(map.values());

        // create ArrayList<UICostOverview> with header for the balances of the loggedInUser with the other group members
        final ArrayList<UICostOverview> listBalance = this.calculateBalances(CostsActivity.groupid, map);
        listBalance.add(0, new UICostOverview("Your balance with...", -1, OverviewAdapter.TYPE_HEADER));

        mAdapter = new OverviewAdapter(getActivity().getApplicationContext(), listAmount, listBalance);
        mRecyclerView.setAdapter(mAdapter);

        //use swipe-to-refresh
        swipeLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);


        return v;
    }

    @Override
    public void onGroupDataAvailable(long groupid) {
        //create ArrayList<UICostOverview> with header for the total expenses per member
        HashMap<Long, UICostOverview> map = this.calculateTotalExpensesPerMember(CostsActivity.groupid);
        ArrayList<UICostOverview> listAmount = new ArrayList<>();
        listAmount.add(new UICostOverview("Who payed how much?", -1, OverviewAdapter.TYPE_HEADER));
        listAmount.addAll(map.values());

        // create ArrayList<UICostOverview> with header for the balances of the loggedInUser with the other group members
        final ArrayList<UICostOverview> listBalance = this.calculateBalances(CostsActivity.groupid, map);
        listBalance.add(0, new UICostOverview("Your balance with...", -1, OverviewAdapter.TYPE_HEADER));

        this.swipeLayout.setRefreshing(false);

        mAdapter.setDataSets(listAmount, listBalance, true);
    }

    private ArrayList<UICostOverview> calculateBalances(long groupid, HashMap<Long, UICostOverview> memberExp){
        ArrayList<UICostOverview> list = new ArrayList<>();

        //how much does everyone get from the others? (Simple in this case because everyone always takes part in the costs)
        HashMap<Long, Double> whatDoesEveryMemberGet = new HashMap<>();
        for (Long userid : memberExp.keySet()){
            UICostOverview totalExpensesOfUser = memberExp.get(userid);
            whatDoesEveryMemberGet.put(userid, totalExpensesOfUser.getAmount() / memberExp.values().size());
        }

        //extract the amount I should get from the others
        double whatDoIGet = whatDoesEveryMemberGet.get(User.getLoggedInUser().getId());

        //delete the just extracted amount. So only the other members' amounts are left
        whatDoesEveryMemberGet.remove(User.getLoggedInUser().getId());

        //calculate my balance with the other members and create response-list
        final HashMap<Long, User> members = Group.myGroups.get(groupid).getMembers();
        for (Long userid : whatDoesEveryMemberGet.keySet()) {
            String name = members.get(userid).getName();
            double balance = whatDoIGet - whatDoesEveryMemberGet.get(userid);
            UICostOverview uiCostOverview = new UICostOverview(name, balance, OverviewAdapter.TYPE_ITEM);
            list.add(uiCostOverview);
        }

        //sort the response-list
        Collections.sort(list, new CustomComparator());

        return list;
    }

    private HashMap<Long, UICostOverview> calculateTotalExpensesPerMember(long groupid){
        HashMap<Long, UICostOverview> map = new HashMap<>();
        final HashMap<Long, User> members = Group.myGroups.get(groupid).getMembers();

        //fill map with initial values
        for (User u: members.values()){
            UICostOverview c;
            if (u.getId() == User.getLoggedInUser().getId()){
                c = new UICostOverview("You", 0.0, OverviewAdapter.TYPE_ITEM);
            }
            else {
                c = new UICostOverview(u.getName(), 0.0, OverviewAdapter.TYPE_ITEM);
            }
            map.put(u.getId(), c);
        }

        //calculate sums
        final HashMap<Long, CostItem> costitems = Group.myGroups.get(groupid).getCostitems();
        for (CostItem item: costitems.values()){
            UICostOverview c = map.get(item.getPayedById());
            c.setAmount(c.getAmount() + item.getAmount());
        }

        return map;
    }

    @Override
    public void onRefresh() {
        ((CostsActivity)getActivity()).refreshGroup();
    }

    public SwipeRefreshLayout getSwipeLayout() {
        return swipeLayout;
    }

    private class CustomComparator implements Comparator<UICostOverview> {
        @Override
        public int compare(UICostOverview o1, UICostOverview o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }
}
