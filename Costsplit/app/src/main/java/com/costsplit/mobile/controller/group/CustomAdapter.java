package com.costsplit.mobile.controller.group;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.costsplit.mobile.costsplit.R;
import java.util.ArrayList;


/**
 * Created by D056691 on 31.03.2015.
 */
public class CustomAdapter extends BaseAdapter {
    // store the context (as an inflated layout)
    private LayoutInflater inflater;
    // store the resource (typically list_item.xml)
    private int resource;
    // store (a reference to) the data
    private ArrayList<ListItem> data;

    // store color references and their order
//    private int[] colors200 = {R.color.blueLight02, R.color.blueLight05, R.color.blueLight04, R.color.blueLight07, R.color.blueLight03, R.color.blueLight06, R.color.blueLight08};
    private int[] colors200 = {R.color.md_red_200, R.color.md_cyan_200, R.color.md_teal_200, R.color.md_green_200, R.color.md_lime_200};
    private int[] colors100 = {R.color.md_red_100, R.color.md_cyan_100, R.color.md_teal_100, R.color.md_green_100, R.color.md_lime_100};

    /**
     * Default constructor. Creates the new Adaptor object to
     * provide a ListView with data.
     * @param context
     * @param resource
     * @param data
     */
    public CustomAdapter(Context context, int resource, ArrayList<ListItem> data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.resource = resource;
        this.data = data;
    }

    /**
     * Return the size of the data set.
     */
    public int getCount() {
        return this.data.size();
    }

    /**
     * Return an object in the data set.
     */
    public Object getItem(int position) {
        return this.data.get(position);
    }

    /**
     * Return the position provided.
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Return a generated view for a position.
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        // reuse a given view, or inflate a new one from the xml
        View view;

        if (convertView == null) {
            view = this.inflater.inflate(resource, parent, false);
        } else {
            view = convertView;
        }

        // bind the data to the view object
        return this.bindData(view, position);

    }

    /**
     * Bind the provided data to the view.
     * This is the only method not required by base adapter.
     */
    public View bindData(View view, int position) {
        // make sure it's worth drawing the view
        if (this.data.get(position) == null) {
            return view;
        }
        //color the view
        view.setBackgroundResource(this.colors100[position % this.colors100.length]);


        // pull out the object
        ListItem item = this.data.get(position);

        // extract the view object
        View viewElement = view.findViewById(R.id.GroupName);
        // cast to the correct type
        TextView tv = (TextView)viewElement;
        // set the value
        tv.setText(item.GroupName);

        viewElement = view.findViewById(R.id.GroupMemberNumber);
        tv = (TextView)viewElement;
        tv.setText(item.GroupMemberNumber);

        tv = (TextView)view.findViewById(R.id.GroupTotalExpenses);
        tv.setText(item.totalExp+"€");

        // return the final view object
        return view;
    }

    public void setData(ArrayList<ListItem> data, boolean refresh) {
        this.data = data;
        if (refresh){
            this.notifyDataSetChanged();
        }
    }
}