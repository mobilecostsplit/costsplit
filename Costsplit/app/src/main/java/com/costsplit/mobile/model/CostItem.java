package com.costsplit.mobile.model;

import java.io.Serializable;
import java.text.DecimalFormat;

/**
 * Created by Timo on 09.04.15.
 */
public class CostItem implements Serializable{
    private long id;
    private String payedByName;
    private long payedById;
    private String title;
    private double amount;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = Double.valueOf(new DecimalFormat("#.##").format(amount));
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPayedByName() {
        return payedByName;
    }

    public void setPayedByName(String payedByName) {
        this.payedByName = payedByName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getPayedById() {
        return payedById;
    }

    public void setPayedById(long payedById) {
        this.payedById = payedById;
    }
}
