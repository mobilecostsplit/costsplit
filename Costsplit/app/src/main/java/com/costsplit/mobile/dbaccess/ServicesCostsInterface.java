package com.costsplit.mobile.dbaccess;

import com.costsplit.mobile.model.uielements.UICostItem;

import java.util.ArrayList;

/**
 * Created by Timo on 09.04.15.
 */
public interface ServicesCostsInterface{
    public void onGroupDataAvailable(long groupid);
}
