package com.costsplit.mobile.dbaccess;

import android.content.Context;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;

/**
 * Created by Timo on 30.03.15.
 * Use this class to access DBHelper-Methods.
 * This class acts as the interface to the controllers. Do not try to access DBHelper-Methods
 * from the controllers directly!
 */
public class ServicesGroup {
    /*
     * ----------------GET SERVICES----------------
     */

    public static void getUserByEmail(String email, AsyncHttpResponseHandler handler){
        DBHelper.getEntity(DBHelper.URL_USER, DBHelper.FILTER_EMAIL, email, handler);
    }

    public static void getGroupIDByName(String name, AsyncHttpResponseHandler handler){
        DBHelper.getEntity(DBHelper.URL_USER, DBHelper.FILTER_EMAIL, name, handler);
    }

    public static void getUserByGroupID(String grID, AsyncHttpResponseHandler handler) {
        DBHelper.getEntity(DBHelper.URL_GROUP_ASSIGNMENT, DBHelper.FILTER_GROUPID, grID, handler);
    }

    public static void getGroupByGroupId(String groupId, AsyncHttpResponseHandler handler) throws JSONException {
        DBHelper.getEntity(DBHelper.URL_GROUP + "?id="+groupId,DBHelper.NO_FILTER,null, handler);
    }

    public static void getGroupAssignmentByGroupId(String groupId, AsyncHttpResponseHandler handler) {
        DBHelper.getEntity(DBHelper.URL_GROUP_ASSIGNMENT, "groupid", groupId, handler);
    }

    public static void getGroupAssignmentByUserId(String userId, AsyncHttpResponseHandler handler) {
        DBHelper.getEntity(DBHelper.URL_GROUP_ASSIGNMENT, "userid", userId, handler);
    }

    public static void getUserById(String userid, AsyncHttpResponseHandler handler) {
        DBHelper.getEntity(DBHelper.URL_USER + "?id="+userid,DBHelper.NO_FILTER,null,handler);
    }


    /*
     * ----------------POST SERVICES----------------
     */

    /*
     * ----------------PUT SERVICES----------------
     */
    public static void createGroup(Context context, String name, AsyncHttpResponseHandler handler){
        try {
            DBHelper.addGroup(context, name, handler);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void assignUserToGroup(Context context, String id, String groupid, AsyncHttpResponseHandler handler){
        //get user by email to retrieve userid

        //use userid and groupid to assign user to group
        try {
            DBHelper.addGroupAssignment(context, id, groupid, handler);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    /*
     * ----------------DELETE SERVICES----------------
     */

    public static void deleteGroupById(Context context, AsyncHttpResponseHandler handler, long id) throws JSONException {
        DBHelper.deleteGroup(context, handler,id);
    }

    /*
     * ----------------OTHER SERVICES----------------
     */
}
