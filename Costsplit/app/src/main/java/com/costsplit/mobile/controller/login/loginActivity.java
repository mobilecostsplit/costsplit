package com.costsplit.mobile.controller.login;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.costsplit.mobile.controller.ActivityConnections;
import com.costsplit.mobile.costsplit.R;
import com.costsplit.mobile.dbaccess.DBHelper;
import com.costsplit.mobile.dbaccess.ServicesLogin;
import com.costsplit.mobile.model.User;
import com.costsplit.mobile.util.Utility;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// http://community.skype.com/t5/image/serverpage/image-id/50473i26CE13D12E32E2CB/image-size/medium?v=mpbl-1&px=-1
public class loginActivity extends ActionBarActivity {

    private EditText emailAdrTxt;
    private EditText pwTxt;
    private Button loginBtn;
    private Button registerBtn;
    private Button pwForgottenBtn;

    private int idRegisterItem = Menu.FIRST;
    private int idPwForgottonItem = Menu.FIRST + 1;


    protected void onRestart(){
        this.pwTxt.setText("");
        super.onRestart();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= 21) {
            this.tintSystemBar();
        }

        setContentView(R.layout.activity_login);

        //Toolbar
//        Toolbar toolbar = (Toolbar)findViewById(R.id.tool_bar);
//        System.out.println("Toolbar: "+toolbar);
//        setSupportActionBar(toolbar);
//        toolbar.setTitle("Titel");

        //get references of UI-elementsAmount
        this.emailAdrTxt = (EditText) findViewById(R.id.emailAdrTxt);
        this.pwTxt = (EditText) findViewById(R.id.pwTxt);
        this.loginBtn = (Button) findViewById(R.id.loginBtn);
        this.registerBtn = (Button) findViewById(R.id.registerBtn);
        this.pwForgottenBtn = (Button) findViewById(R.id.pwForgottenBtn);

        //set click listeners
        this.loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logIn();
            }
        });
        this.registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callRegisterView();
            }
        });
        this.pwForgottenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pwforgotten();
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void tintSystemBar() {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        window.setStatusBarColor(getResources().getColor(R.color.blueDark03));
    }

    private void pwforgotten() {
        System.out.println("Move to pw-forgotten screen...");
        Intent i = new Intent(getApplicationContext(), ActivityConnections.PWFORGOTTEN_PATH);
        startActivity(i);
    }

    private void callRegisterView() {
        //move to second screen
        System.out.println("Move to register screen...");
        Intent i = new Intent(getApplicationContext(), ActivityConnections.REGISTER_PATH);
        startActivity(i);
    }

    private void logIn() {
        String email = this.emailAdrTxt.getText().toString();
        final String pw = this.pwTxt.getText().toString();

        //validation
        if(!(Utility.isNotNull(email) && Utility.isNotNull(pw))){
            return;
        }
        if(!Utility.validate(email)){
            Toast.makeText(getApplicationContext(), "Please enter a valid email.", Toast.LENGTH_LONG).show();
            return;
        }

        //if pw shall be saved --> save them!


        //create loggedInUser-Object
        final User loggedInUser = new User();
        loggedInUser.setEmail(email);

        //create handler

        final AsyncHttpResponseHandler loginHandler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                User.setLoggedInUser(loggedInUser);
                //move to second screen
                Intent i = new Intent(getApplicationContext(), ActivityConnections.ShowGroups_Path);
                startActivity(i);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getApplicationContext(), "Wrong email or password entered! Try again.", Toast.LENGTH_LONG).show();
            }
        };

        ServicesLogin.loginUser(getApplicationContext(), loggedInUser, pw, loginHandler);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        menu.add(1, idRegisterItem, idRegisterItem, "Register");
//        menu.add(1, idPwForgottonItem, idPwForgottonItem, "Password forgotten?");

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        else if (id == idRegisterItem){
//            this.callRegisterView();
//            return true;
//        }
//        else if (id == idPwForgottonItem){
//            this.pwforgotten();
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }
}
