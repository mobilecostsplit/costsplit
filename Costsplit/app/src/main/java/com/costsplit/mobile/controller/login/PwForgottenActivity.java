package com.costsplit.mobile.controller.login;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.costsplit.mobile.costsplit.R;
import com.costsplit.mobile.dbaccess.ServicesLogin;
import com.costsplit.mobile.util.Utility;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

public class PwForgottenActivity extends Activity {

    private Button pwForgottenBtn;
    private EditText emailAdrTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 21) {
            this.tintSystemBar();
        }
        setContentView(R.layout.activity_pw_forgotten);

        this.pwForgottenBtn = (Button) findViewById(R.id.pwForgottenBtn);
        this.emailAdrTxt = (EditText) findViewById(R.id.emailAdrPwForgotTxt);

        this.pwForgottenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pwForgotten();
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void tintSystemBar() {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        window.setStatusBarColor(getResources().getColor(R.color.blueDark03));
    }

    private void pwForgotten() {
        String email = this.emailAdrTxt.getText().toString();

        //validation
        if(!(Utility.isNotNull(email))){
            return;
        }
        if(!Utility.validate(email)){
            return;
        }

        AsyncHttpResponseHandler handler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Toast.makeText(getApplicationContext(), "Password sent to your email address!", Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_LONG).show();
            }
        };

        ServicesLogin.pwForgottenEmail(this, email, handler);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pw_forgotten, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
