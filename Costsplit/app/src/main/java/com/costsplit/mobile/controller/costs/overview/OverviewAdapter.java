package com.costsplit.mobile.controller.costs.overview;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.costsplit.mobile.costsplit.R;
import com.costsplit.mobile.model.uielements.UICostOverview;

import java.util.ArrayList;

/**
 * Created by Timo on 07.04.15.
 */
public class OverviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<UICostOverview> itemSetAmounts;
    private ArrayList<UICostOverview> itemSetBalance;
    private Context context;
    public static final int TYPE_HEADER = 10;
    public static final int TYPE_ITEM = 20;


    private int headersAlreadyPlaced = 0;

    public OverviewAdapter(Context context, ArrayList<UICostOverview> itemSetAmounts, ArrayList<UICostOverview> itemSetBalance) {
        this.itemSetAmounts = itemSetAmounts;
        this.itemSetBalance = itemSetBalance;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
       if(position < itemSetAmounts.size()){
           return itemSetAmounts.get(position).getType();
       }
       else{
           position -= itemSetAmounts.size();
           return itemSetBalance.get(position).getType();
       }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.tab_overview_listitem, parent, false);

            ViewHolderItem vh = new ViewHolderItem(v);
            return vh;
        }
        else{
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.tab_overview_listheader, parent, false);

            ViewHolderHeader vh = new ViewHolderHeader(v);
            return vh;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ArrayList<UICostOverview> curDataSet;
        if (position < this.itemSetAmounts.size()){
            curDataSet = this.itemSetAmounts;
        }
        else{
            position -= this.itemSetAmounts.size();
            curDataSet = this.itemSetBalance;
        }

        if(holder instanceof ViewHolderHeader){
            ViewHolderHeader headerHolder = (ViewHolderHeader) holder;
            headerHolder.headerTxt.setText(curDataSet.get(position).getName());
        }
        else{
            ViewHolderItem itemHolder = (ViewHolderItem) holder;
            itemHolder.nameTxt.setText(curDataSet.get(position).getName());
            itemHolder.amountTxt.setTextColor(this.context.getResources().getColor(R.color.darkgrey));
            double am = curDataSet.get(position).getAmount();
            if (am < 0){
                itemHolder.amountTxt.setText(am+"€");
                itemHolder.amountTxt.setTextColor(this.context.getResources().getColor(R.color.md_red_700));
            }
            else{
                if (curDataSet == itemSetBalance) {
                    itemHolder.amountTxt.setText("+"+am+"€");
                    itemHolder.amountTxt.setTextColor(this.context.getResources().getColor(R.color.md_green_700));
                }
                else{
                    itemHolder.amountTxt.setText(am+"€");
                }
            }
        }

    }

    @Override
    public int getItemCount() {
        return (this.itemSetAmounts.size()+this.itemSetBalance.size());
    }

    public static class ViewHolderItem extends RecyclerView.ViewHolder {
        //textview only for testing purposes
//        public TextView mTextView;
        public View v;
        public TextView nameTxt;
        public TextView amountTxt;

        public ViewHolderItem(View v) {
            super(v);
            this.v = v;
            this.nameTxt = (TextView)v.findViewById(R.id.tab_overview_nameTxt);
            this.amountTxt = (TextView)v.findViewById(R.id.tab_overview_amountTxt);
        }
    }

    public static class ViewHolderHeader extends RecyclerView.ViewHolder {
        //textview only for testing purposes
//        public TextView mTextView;
        public View v;
        public TextView headerTxt;

        public ViewHolderHeader(View v) {
            super(v);
            this.v = v;
            this.headerTxt = (TextView)v.findViewById(R.id.tab_overview_headerTxt);
        }
    }

    public ArrayList<UICostOverview> getItemSetAmounts() {
        return itemSetAmounts;
    }

    public void setItemSetAmounts(ArrayList<UICostOverview> itemSetAmounts) {
        this.itemSetAmounts = itemSetAmounts;
    }

    public ArrayList<UICostOverview> getItemSetBalance() {
        return itemSetBalance;
    }

    public void setItemSetBalance(ArrayList<UICostOverview> itemSetBalance) {
        this.itemSetBalance = itemSetBalance;
    }

    public void setDataSets(ArrayList<UICostOverview> itemSetAmounts, ArrayList<UICostOverview> itemSetBalance, boolean refresh){
        this.itemSetBalance = itemSetBalance;
        this.itemSetAmounts = itemSetAmounts;
        if (refresh){
            this.notifyDataSetChanged();
        }
    }
}
