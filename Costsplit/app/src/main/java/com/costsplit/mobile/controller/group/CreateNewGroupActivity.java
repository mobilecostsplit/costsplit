package com.costsplit.mobile.controller.group;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.costsplit.mobile.controller.costs.overview.CostsActivity;
import com.costsplit.mobile.costsplit.R;
import com.costsplit.mobile.dbaccess.ServicesCosts;
import com.costsplit.mobile.dbaccess.ServicesCostsInterface;
import com.costsplit.mobile.dbaccess.ServicesGroup;
import com.costsplit.mobile.model.Group;
import com.costsplit.mobile.model.User;
import com.costsplit.mobile.util.Utility;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ulrichwiebach on 31.03.15.
 */
public class CreateNewGroupActivity extends ActionBarActivity implements ServicesCostsInterface{

    private EditText groupname;
    private Button saveBtn;
    public static String grID;
    boolean addMemberallowed = false;
    private Toolbar toolbar;
    private int id_backbutton = 16908332;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_group);

        // Toolbar einfaerben
        if (Build.VERSION.SDK_INT >= 21) {
            this.tintSystemBar();
        }

        //Toolbar
        this.toolbar = (Toolbar)findViewById(R.id.edit_group_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Create Group");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //get references of UI-elementsAmount
        this.groupname = (EditText) findViewById(R.id.edit_groupname);
        this.saveBtn = (Button) findViewById(R.id.createButton);

//        set click listeners
        this.saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Save Group
                saveGroup();
            }
        });

    }

    private void saveGroup() {
        String groupName = this.groupname.getText().toString();

        //validation
        if(!(Utility.isNotNull(groupName))){
            return;
        }


        //create handler
        final AsyncHttpResponseHandler groupNameHandler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                  try {
                    System.out.println(new String(responseBody));
                    JSONObject json = new JSONObject(new String(responseBody));
                    grID = json.getString("Id");
                    System.out.println(grID);


                      String userId = String.valueOf(User.getLoggedInUser().getId());
                      assignMember(userId, grID);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        };


        ServicesGroup.createGroup(getApplicationContext(), groupName, groupNameHandler);

    }


    public void assignMember(String id, String groupid){
        final AsyncHttpResponseHandler assignMemberHandler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                long groupID = Long.parseLong(grID);
                ServicesCosts.getGroupById(groupID, false, com.costsplit.mobile.controller.group.CreateNewGroupActivity.this);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        };
        ServicesGroup.assignUserToGroup(getApplicationContext(), id, groupid, assignMemberHandler);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_new_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if (id == this.id_backbutton){
            setResult(MyGroupsActivity.RESP_CANCEL);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onGroupDataAvailable(long groupid) {
        System.out.println("BIN HIER!");
        long groupID = Long.valueOf(grID).longValue();
        System.out.println(Group.myGroups.get(groupID).getMembers().toString());
        addMemberallowed = true;
        Intent i = new Intent(getApplicationContext(), EditNewGroupActivity.class);
        startActivityForResult(i, MyGroupsActivity.REQ_NEW_GROUP);
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void tintSystemBar() {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        window.setStatusBarColor(getResources().getColor(R.color.blueDark03));
    }

}
