package com.costsplit.mobile.controller.group;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.costsplit.mobile.controller.ActivityConnections;
import com.costsplit.mobile.controller.costs.EditCost;
import com.costsplit.mobile.controller.costs.NewCost;
import com.costsplit.mobile.costsplit.R;
import com.costsplit.mobile.dbaccess.ServicesCosts;
import com.costsplit.mobile.dbaccess.ServicesCostsInterface;
import com.costsplit.mobile.dbaccess.ServicesGroup;
import com.costsplit.mobile.model.CostItem;
import com.costsplit.mobile.model.Group;
import com.costsplit.mobile.model.User;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.software.shell.fab.ActionButton;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class MyGroupsActivity extends ActionBarActivity implements ServicesCostsInterface, SwipeRefreshLayout.OnRefreshListener{

    private ImageButton myProfileBtn;
    private ImageButton myBattlesBtn;
    private Button newGroupBtn;
    private CustomAdapter adapter;
    final ArrayList<ListItem> data = new ArrayList<ListItem>();
    private Toolbar toolbar;
    private int id_backbutton = 16908332;
    private SwipeRefreshLayout swipeLayout;
    private int totalCountGroups = 0;
    private int curCountGroups = 0;

    public static final int REQ_COSTS_GROUP = 150;
    public static final int REQ_NEW_GROUP = 160;
    public static final int RESP_COSTS_GROUP = 151;
    public static final int RESP_CANCEL = 152;
    public static final int RESP_NEW_GROUP = 161;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Toolbar einfaerben
        if (Build.VERSION.SDK_INT >= 21) {
            this.tintSystemBar();
        }

        setContentView(R.layout.activity_my_groups);

        //Toolbar
        this.toolbar = (Toolbar)findViewById(R.id.groups_tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Your Groups");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Floating Action Button
        ActionButton b = (ActionButton)findViewById(R.id.newGroupBtn);
        b.setButtonColor(getResources().getColor(R.color.standard_orange));
        b.setButtonColorPressed(getResources().getColor(R.color.orangeDark02));
        b.setImageResource(R.drawable.fab_plus_icon);
        b.setShadowRadius(5.0f);
        b.setShadowYOffset(3.0f);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), CreateNewGroupActivity.class);
                startActivityForResult(i, REQ_NEW_GROUP);
            }
        });

        adapter = new CustomAdapter(this, R.layout.listofgroupslayout, new ArrayList<ListItem>());
        ListView myList = (ListView) findViewById(R.id.listOfMyGroups);
        myList.setAdapter(adapter);
        myList.setTextFilterEnabled(true);
        myList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // testweise einen toast ausgeben, hier eigentlich zur neuen activity mit der ID
                Intent i = new Intent(getApplicationContext(), ActivityConnections.COSTSACTIVITY_PATH);
                i.putExtra("groupid",parent.getItemAtPosition(position).toString());
                startActivityForResult(i, REQ_COSTS_GROUP);
            }
        });
        /*
        // hier koennte man die swipe funktion einbauen
        myList.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {
            @Override
            public void onSwipeLeft() {
                Toast.makeText(getApplicationContext(), "link"+parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
                //gruppeLoeschen(3246728369823L);
            }
        });
        */

        // aufgrund der komplexitat wird zum loeschen der lange klick verwendet
        myList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getApplicationContext(), "lange" + parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
                long gebe = Long.parseLong(parent.getItemAtPosition(position).toString());
                gruppeLoeschen(gebe);
                return true;
            }
        });

        //use swipe-to-refresh
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);

        datenLaden();
    }

//    protected void onResume(){
//        super.onResume();
//        System.out.println("executing on Resume on MyGroupsActivity");
//        boolean b = getIntent().getBooleanExtra("newgroup", false);
//        System.out.println("'newgroup' extra with value: "+b);
//        if (b){
//            this.onRefresh();
//        }
//    }

    // HIER AUCH NOCH DAS GROUP ASSIGNMENT LOESCHEN!!!
    public void gruppeLoeschen(long id) {
        final AsyncHttpResponseHandler deleteGroupByIdHandler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                //Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_LONG).show();
                String resp = new String(responseBody);
                try {
                    JSONObject da = new JSONObject(resp);
                    if(da.getBoolean("Deleted")) {
                        datenLaden();
                    } else {
                        Toast.makeText(getApplicationContext(), "Gruppe konnte nicht gelöscht werden!", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_LONG).show();
            }
        };

        // hier den service aufrufen
        try {
            ServicesGroup.deleteGroupById(getApplicationContext(),deleteGroupByIdHandler,id);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }

    public void datenLaden() {
        swipeLayout.setRefreshing(true);

        final MyGroupsActivity This = this;
        // Daten laden und view refreshen
        final AsyncHttpResponseHandler getGroupAssignmentByUserIdHandler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                //Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_LONG).show();
                String resp = new String(responseBody);

                try {
                    JSONArray arr = new JSONArray(resp);
                    totalCountGroups = arr.length();
                    data.clear();
                    //System.out.println(arr.toString());
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject da = (JSONObject) arr.get(i);
                        long hol_groupId = Long.parseLong(da.get("groupid").toString());

                        //System.out.println("hol_groudId:"+hol_groupId);
                        ServicesCosts.getGroupById(hol_groupId,true,This);
                    }
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_LONG).show();
            }
        };

        // hier den service aufrufen
        try {
            // Group Assignment fuer den user holen
            //String userId = "6264504945999872";
            String userId = String.valueOf(User.getLoggedInUser().getId());
            ServicesGroup.getGroupAssignmentByUserId(userId,getGroupAssignmentByUserIdHandler);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_groups, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if (id == this.id_backbutton){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void tintSystemBar() {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        window.setStatusBarColor(getResources().getColor(R.color.blueDark03));
    }

    @Override
    public void onGroupDataAvailable(long groupid) {
        // do not add them immediately to view. we need to sort them before!
        Group g = Group.myGroups.get(groupid);
        data.add(new ListItem(g.getName(), g.getMembers().size() + " Members", this.calcSum(groupid), groupid));

        // did we receive all group data yet?
        curCountGroups++;
        if (totalCountGroups == curCountGroups){
            totalCountGroups = 0; //reset the values, they are only needed so set the refresh-indicator
            curCountGroups = 0;

            Collections.sort(data, new CustomComparator());

            ArrayList<ListItem> items = new ArrayList<>();
            items.addAll(data);
            adapter.setData(items, true);

            swipeLayout.setRefreshing(false);
        }
    }

    @Override
    public void onRefresh() {
        datenLaden();
    }

    private double calcSum(long groupid){
        double sum = 0;
        Collection<CostItem> list = Group.myGroups.get(groupid).getCostitems().values();

        for (CostItem item : list){
            sum += item.getAmount();
        }
        return Double.valueOf(new DecimalFormat("#.##").format(sum));
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        System.out.println("OnActivityResult with req_code: "+requestCode+" and resultCode: "+resultCode);
        switch(requestCode) {
            case (REQ_COSTS_GROUP) : {
                if (resultCode == RESP_COSTS_GROUP) {
                    this.onRefresh();
                }
                else if (resultCode == RESP_CANCEL){
                    // do nothing
                }
                break;
            }
            case(REQ_NEW_GROUP) : {
                if (resultCode == RESP_CANCEL){
                    // do nothing
                }
                else{
                    this.onRefresh();
                }
                break;
            }
        }
    }

    private class CustomComparator implements java.util.Comparator<ListItem> {
        @Override
        public int compare(ListItem lhs, ListItem rhs) {
            return lhs.GroupName.compareTo(rhs.GroupName);
        }
    }
}
