package com.costsplit.mobile.controller.costs;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.costsplit.mobile.controller.costs.overview.CostsActivity;
import com.costsplit.mobile.controller.group.ListItem;
import com.costsplit.mobile.controller.group.MyGroupsActivity;
import com.costsplit.mobile.costsplit.R;
import com.costsplit.mobile.dbaccess.ServicesCosts;
import com.costsplit.mobile.dbaccess.ServicesGroup;
import com.costsplit.mobile.model.Group;
import com.costsplit.mobile.model.User;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NewCost extends ActionBarActivity implements AdapterView.OnItemSelectedListener {

    Button saveCostButton;
    EditText costTitleField;
    EditText costAmountField;
    Spinner paidBySpinner;
    String paidBy;
    String[] userIdValues;
    ArrayAdapter<String> userOfGroupAdapter;
    private Toolbar toolbar;
    private int id_backbutton = 16908332;
    public static final int RESULT_NEW_EXPENSE_ADDED = 111;
    public static final int RESULT_CANCEL = 112;



    // GruppenID aus dem Model auslesen, wird sie denn da gespeichert?
    // Timo fragen, denn von seinerm screen kommt man hier her
    // Hier testweise feste ID, damit die Teilnehmer der Gruppe ausgelesen werden koennen
//    String groupId = "5390255762964480";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Toolbar einfaerben
        if (Build.VERSION.SDK_INT >= 21) {
            this.tintSystemBar();
        }

        setContentView(R.layout.activity_new_cost);

        //Toolbar
        this.toolbar = (Toolbar)findViewById(R.id.new_costs_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("New Expense");


        // Wenn auf speichern geklickt wurde
        this.saveCostButton = (Button) findViewById(R.id.saveCostButton);
        this.saveCostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveNewCostitem();
            }
        });

        final HashMap<Long, User> members = Group.myGroups.get(CostsActivity.groupid).getMembers();
        ArrayList<String> usernames = new ArrayList<>();
        usernames.add("Who payed for it?");
        this.userIdValues = new String[members.size()];
        int position = 0;
        for(User u: members.values()){
            usernames.add(u.getName());
            this.userIdValues[position] = String.valueOf(u.getId());
            position++;
        }

        paidBySpinner = (Spinner) findViewById(R.id.paidBySpinner);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, usernames);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        paidBySpinner.setAdapter(arrayAdapter);

    }

    private void saveNewCostitem() {
        // Daten auslesen
        costTitleField = (EditText) findViewById(R.id.costTitleField);
        String costTitle = costTitleField.getText().toString();
        costAmountField = (EditText) findViewById(R.id.costAmountField);
        double costAmount = Double.parseDouble(costAmountField.getText().toString());

        if(costTitle.equals("")){
            Toast.makeText(getApplicationContext(), "What did you buy?", Toast.LENGTH_LONG).show();
            return;
        }
        if (costAmountField.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), "How much was it?", Toast.LENGTH_LONG).show();
            return;
        }
        if (paidBySpinner.getSelectedItemPosition() == 0){
            Toast.makeText(getApplicationContext(), "Who payed for it?", Toast.LENGTH_LONG).show();
            return;
        }

        paidBy = userIdValues[paidBySpinner.getSelectedItemPosition()-1];
        System.out.println(paidBy);

        // Daten wegspeichern
        final AsyncHttpResponseHandler saveNewCostHandler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                setResult(RESULT_NEW_EXPENSE_ADDED);
                finish();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_LONG).show();
            }
        };

        try {
            ServicesCosts.saveNewCost(getApplicationContext(), paidBy, String.valueOf(CostsActivity.groupid), costAmount, costTitle, saveNewCostHandler);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_new_cost, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cancel) {
            setResult(RESULT_CANCEL);
            finish();
        }
        else if (id == this.id_backbutton){
            setResult(RESULT_CANCEL);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void tintSystemBar() {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        window.setStatusBarColor(getResources().getColor(R.color.blueDark03));
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        paidBySpinner.setSelection(position);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}