package com.costsplit.mobile.controller.costs.overview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.costsplit.mobile.controller.ActivityConnections;
import com.costsplit.mobile.costsplit.R;
import com.costsplit.mobile.dbaccess.ServicesCosts;
import com.costsplit.mobile.dbaccess.ServicesCostsInterface;
import com.costsplit.mobile.model.CostItem;
import com.costsplit.mobile.model.Group;
import com.costsplit.mobile.model.uielements.UICostItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Created by Timo on 31.03.15.
 */
public class TabCosts extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
        ServicesCostsInterface, CostsItemsAdapter.ClickListener {
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private CostsItemsAdapter mAdapter;
    private SwipeRefreshLayout swipeLayout;
    private ArrayList<CostItem> listCostItems;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_costs,container,false);

        mRecyclerView = (RecyclerView)v.findViewById(R.id.CostsItemsRecyclerView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        //create sample data
//        UICostItem.addElement("03.03.15 Beer", 15.23, "Timo", "everyone");
//        UICostItem.addElement("04.03.15 More Beer", 30.46, "Yannick", "everyone");


        mAdapter = new CostsItemsAdapter(new ArrayList<CostItem>());
        mRecyclerView.setAdapter(mAdapter);

        //use swipe-to-refresh
        swipeLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);

        //set data to recyclerview
//        System.out.println(Group.myGroups.get(CostsActivity.groupid).getCostitems());
        HashMap<Long, CostItem> costitems = Group.myGroups.get(CostsActivity.groupid).getCostitems();
        ArrayList<CostItem> arrCostItems = new ArrayList<CostItem>(costitems.values());
        Collections.sort(arrCostItems, Collections.reverseOrder(new CustomComparator()));
        mAdapter.setDataSet(arrCostItems, true);

        mAdapter.setClickListener(this);

        this.listCostItems = arrCostItems;

        return v;
    }

    @Override
    public void onRefresh() {
        ((CostsActivity)getActivity()).refreshGroup();
    }

    @Override
    public void onGroupDataAvailable(long groupid) {
        HashMap<Long, CostItem> costitems = Group.myGroups.get(groupid).getCostitems();
        ArrayList<CostItem> arrCostItems = new ArrayList<CostItem>(costitems.values());
        Collections.sort(arrCostItems, Collections.reverseOrder(new CustomComparator()));
        mAdapter.setDataSet(arrCostItems, true);
        this.swipeLayout.setRefreshing(false);

        this.listCostItems = arrCostItems;
    }

    @Override
    public void itemClicked(View v, int position) {
        CostItem item = this.listCostItems.get(position);
        Intent i = new Intent(getActivity().getApplicationContext(), ActivityConnections.EDITCOST_PATH);
        i.putExtra("item", item);
        getActivity().startActivityForResult(i, CostsActivity.REQ_CODE_EDIT_EXPENSE);
    }

    public SwipeRefreshLayout getSwipeLayout() {
        return swipeLayout;
    }

    private class CustomComparator implements Comparator<CostItem> {
        @Override
        public int compare(CostItem o1, CostItem o2) {
            Long l1 = new Long(o1.getId());
            Long l2 = new Long(o2.getId());
            return l1.compareTo(l2);
        }
    }
}
