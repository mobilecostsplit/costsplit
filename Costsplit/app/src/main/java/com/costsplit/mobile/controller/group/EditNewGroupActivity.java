package com.costsplit.mobile.controller.group;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.view.Window;
import android.view.WindowManager;
import android.annotation.TargetApi;

import com.costsplit.mobile.costsplit.R;
import com.costsplit.mobile.dbaccess.ServicesCosts;
import com.costsplit.mobile.dbaccess.ServicesCostsInterface;
import com.costsplit.mobile.dbaccess.ServicesGroup;
import com.costsplit.mobile.model.Group;
import com.costsplit.mobile.model.User;
import com.costsplit.mobile.util.Utility;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ulrichwiebach on 31.03.15.
 */
public class EditNewGroupActivity extends ActionBarActivity implements ServicesCostsInterface{

    private TextView groupname;
    private Button saveBtn;
    private EditText email;
    private Button addMemberBtn;
    private ListView listView;
    private Toolbar toolbar;
    ArrayList<String> values =new ArrayList<String>();
    ArrayAdapter<String> entries;
    private int id_backbutton = 16908332;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_new_group);

        // Toolbar einfaerben
        if (Build.VERSION.SDK_INT >= 21) {
            this.tintSystemBar();
        }

        //Toolbar
        this.toolbar = (Toolbar)findViewById(R.id.edit_costs_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Create Group");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //get references of UI-elementsAmount
        this.groupname = (TextView) findViewById(R.id.edit_costTitleField);
        this.email = (EditText) findViewById(R.id.edit_new_add_group_member);
        this.addMemberBtn = (Button) findViewById(R.id.edit_saveCostButton);
        this.listView = (ListView) findViewById(R.id.listView2);

        this.addMemberBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Add Member to ListView
                    addMember();
            }
        });

        entries = new ArrayAdapter<String>
                (getApplicationContext(), android.R.layout.simple_list_item_1, values){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text = (TextView) view.findViewById(android.R.id.text1);
                text.setTextColor(Color.BLACK);
                return view;
            }
        };

        listView.setAdapter(entries);
        loadGroupName();
        loadListView();

    }

    private void loadListView(){


        // Hashmap
        // In Listview
        long groupID = Long.valueOf(CreateNewGroupActivity.grID).longValue();
        System.out.println("groupID:"+groupID);
        for (User u: Group.myGroups.get(groupID).getMembers().values()){
            String user = u.getName();
            entries.add(user);
        }
    }

    private void loadGroupName(){
        //Groupname aus "Cache"
        long groupID = Long.valueOf(CreateNewGroupActivity.grID).longValue();
        this.groupname.setText(Group.myGroups.get(groupID).getName());
    }

    private void addMember() {
        String email = this.email.getText().toString();
        final String groupName = this.groupname.getText().toString();
        boolean alreadyIn = false;

        //validation
        if(!(Utility.isNotNull(email))){
            Toast.makeText(getApplicationContext(), "Please enter an Email!", Toast.LENGTH_LONG).show();
            return;
        }
        if(!Utility.validate(email)){
            Toast.makeText(getApplicationContext(), "This Email is not valid!", Toast.LENGTH_LONG).show();
            return;
        }


        long groupID = Long.parseLong(CreateNewGroupActivity.grID);
        for (User u: Group.myGroups.get(groupID).getMembers().values()){
            String userMail = u.getEmail();
            if(userMail.equals(email)){
                alreadyIn = true;
            }
            else {
                System.out.println("user:"+userMail);
                System.out.println("email:"+email);
            }
        }

        if(alreadyIn == true){
            Toast.makeText(getApplicationContext(), "This User is already in the List!", Toast.LENGTH_LONG).show();
            return;

        }

        //create handler
        final AsyncHttpResponseHandler emailHandler = new AsyncHttpResponseHandler() {

            //does the user exist?
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String resp = new String(responseBody);
                try {

                    System.out.println(new String(responseBody));
                    JSONArray jsonArr = new JSONArray(new String(responseBody));
                    JSONObject json = jsonArr.getJSONObject(0);
                    String user = json.getString("name");
                    System.out.println(user);



                    //does the user exist in ListView?

                    //add User to ListView
                    entries.add(user);

                    String id = ""+json.getLong("id");
                    assignMember(id, CreateNewGroupActivity.grID );
                    long groupID = Long.valueOf(CreateNewGroupActivity.grID).longValue();
                    ServicesCosts.getGroupById(groupID, false, EditNewGroupActivity.this);

                }
                catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getApplicationContext(), "This User does not exist! Try again.", Toast.LENGTH_LONG).show();
            }
        };


        ServicesGroup.getUserByEmail(email, emailHandler);


    }

    public void assignMember(String id, final String groupId){
        final AsyncHttpResponseHandler assignMemberHandler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                long groupID = Long.parseLong(groupId);
                ServicesCosts.getGroupById(groupID, false, EditNewGroupActivity.this);
                email.setText("");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        };
        ServicesGroup.assignUserToGroup(getApplicationContext(), id, groupId, assignMemberHandler);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_done) {
//            Intent i = new Intent(getApplicationContext(), MyGroupsActivity.class);
//            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            i.putExtra("newgroup", true);
//            startActivity(i);
            setResult(MyGroupsActivity.RESP_NEW_GROUP);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onGroupDataAvailable(long groupid) {

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void tintSystemBar() {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        window.setStatusBarColor(getResources().getColor(R.color.blueDark03));
    }
}
