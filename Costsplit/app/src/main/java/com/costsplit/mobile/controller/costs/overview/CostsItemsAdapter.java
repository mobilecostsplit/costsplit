package com.costsplit.mobile.controller.costs.overview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.costsplit.mobile.costsplit.R;
import com.costsplit.mobile.model.CostItem;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Timo on 09.04.15.
 */
public class CostsItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<CostItem> dataSet;
    public ClickListener clickListener;
    private static int TYPE_HEADER = 10;
    private static int TYPE_ITEM = 20;

    public CostsItemsAdapter(ArrayList<CostItem> dataset){
        this.dataSet = dataset;
    }

    public void setClickListener(ClickListener clickListener){
        this.clickListener = clickListener;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0){
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.tab_costs_listitem, parent, false);

            ViewHolderItem vh = new ViewHolderItem(v);
            return vh;
        }
        else{
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.tab_costs_listheader, parent, false);

            ViewHolderHeader vh = new ViewHolderHeader(v);
            return vh;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderHeader){
            ViewHolderHeader headerHolder = (ViewHolderHeader) holder;
            headerHolder.titleTxt.setText("Total expenses");
            headerHolder.amountTxt.setText(this.getTotalExpenses(this.dataSet)+"€");
        }
        else {
            position--; //first line is a header
            ViewHolderItem itemHolder = (ViewHolderItem) holder;
            CostItem item = this.dataSet.get(position);
            itemHolder.titleTxt.setText(item.getTitle());
            itemHolder.amountTxt.setText(item.getAmount() + "€");
            itemHolder.membersTxt.setText(item.getPayedByName() + " payed for everyone");
        }
    }

    @Override
    public int getItemCount() {
        return this.dataSet.size()+1;
    }

    public double getTotalExpenses(ArrayList<CostItem> list){
        double sum = 0;
        for (CostItem item : list){
            sum += item.getAmount();
        }
        return Double.valueOf(new DecimalFormat("#.##").format(sum));
    }


    class ViewHolderItem extends RecyclerView.ViewHolder implements View.OnClickListener {
        //textview only for testing purposes
//        public TextView mTextView;
        public View v;
        public TextView titleTxt;
        public TextView amountTxt;
        public TextView membersTxt;

        public ViewHolderItem(View v) {
            super(v);
            this.v = v;
            this.titleTxt = (TextView)v.findViewById(R.id.tab_costs_items_title);
            this.amountTxt = (TextView)v.findViewById(R.id.tab_costs_items_price);
            this.membersTxt = (TextView)v.findViewById(R.id.tab_costs_items_members);

            this.v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null){
                clickListener.itemClicked(v, getAdapterPosition()-1);
            }
        }
    }

    public ArrayList<CostItem> getDataSet() {
        return dataSet;
    }

    public void setDataSet(ArrayList<CostItem> dataSet, boolean shallRefresh) {
        this.dataSet = dataSet;
        if (shallRefresh){
            this.notifyDataSetChanged();
        }
    }

    class ViewHolderHeader extends RecyclerView.ViewHolder {
        public TextView titleTxt;
        public TextView amountTxt;

        public ViewHolderHeader(View v) {
            super(v);
            this.titleTxt = (TextView)v.findViewById(R.id.tab_costs_header_title);
            this.amountTxt = (TextView)v.findViewById(R.id.tab_costs_header_amount);
        }
    }

    public interface ClickListener{
        public void itemClicked(View v, int position);
    }

}
