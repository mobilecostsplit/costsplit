package com.costsplit.mobile.dbaccess;

import android.app.Activity;
import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by Timo on 20.03.15.
 */
public class DBHelper {

    //URLs
    public static final String BASE_URL = "http://space-labs.appspot.com/repo/5506741919809536/costsplit/";

    public static final String URL_USER = BASE_URL + "user.sjs";
    public static final String URL_GROUP = BASE_URL + "group.sjs";
    public static final String URL_GROUP_ASSIGNMENT = BASE_URL + "groupassignment.sjs";
    public static final String URL_COST_POSITION = BASE_URL + "costpos.sjs";

    public static final String URL_BALANCE_GROUP = BASE_URL + "groupclear.sjs";
    public static final String URL_LOGIN = BASE_URL + "login.sjs";
    public static final String URL_EMAIL = "http://space-labs.appspot.com/repo/5768310863953920/sendmail.sjs";

    //filters
    public static final String FILTER_NAME = "name";
    public static final String FILTER_EMAIL = "email";
    public static final String FILTER_ID = "id";
    public static final String FILTER_USERID = "userid";
    public static final String FILTER_GROUPID = "groupid";
    public static final String NO_FILTER = "none";


    /* ----------------------------------------------------------------------------------------------------------------
     * Webservices provided by Michael Dietz
     */

    //***********************************PUT ServicesLogin

    /**
     * Webservice returns JSON:
     * { "id": xxxx }
     *
     * @param context
     * @param name
     * @param email
     * @param password
     * @param handler
     * @throws JSONException
     */
    protected static void addUser(Context context, String name, String email, String password, AsyncHttpResponseHandler handler) throws JSONException {
        JSONObject json = new JSONObject();
        json.put("name", name);
        json.put("email", email);
        json.put("password", password);

        performPut(context, URL_USER, json, handler);
    }

    /**
     * Webservice returns JSON:
     * { "id": xxxx }
     *
     * @param context
     * @param name
     * @param handler
     * @throws JSONException
     */
    protected static void addGroup(Context context, String name, AsyncHttpResponseHandler handler) throws JSONException {
        JSONObject json = new JSONObject();
        json.put("name", name);

        performPut(context, URL_GROUP, json, handler);
    }

    /**
     * Webservice returns JSON:
     * { "id": xxxx }
     *
     * @param context
     * @param userid
     * @param groupid
     * @param handler
     * @throws JSONException
     */
    protected  static void addGroupAssignment(Context context, String userid, String groupid, AsyncHttpResponseHandler handler) throws JSONException {
        JSONObject json = new JSONObject();
        json.put("userid", userid);
        json.put("groupid", groupid);

        performPut(context, URL_GROUP_ASSIGNMENT, json, handler);
    }

    /**
     * Webservice returns JSON:
     * { "id": xxxx }
     *
     * @param context
     * @param userid
     * @param groupid
     * @param value
     * @param handler
     * @throws JSONException
     */
    protected static void addCostPosition(Context context, String userid, String groupid, double value, String title, AsyncHttpResponseHandler handler) throws JSONException {
        JSONObject json = new JSONObject();
        json.put("userid", userid);
        json.put("groupid", groupid);
        json.put("value", value);
        json.put("title", title);

        performPut(context, URL_COST_POSITION, json, handler);
    }

    //***********************************POST ServicesLogin

    /**
     * Webservice returns JSON:
     * {"Successful":true,"userid":xxxx }
     *
     * @param context
     * @param id
     * @param password
     * @param handler
     * @throws JSONException
     */
    protected static void loginUser(Context context, long id, String password, AsyncHttpResponseHandler handler) throws JSONException {
        JSONObject json = new JSONObject();
        json.put("id", id);
        json.put("password", password);

        performPost(context, URL_LOGIN, json, handler);
    }

    protected static void clearGroup(Context context, long groupid, AsyncHttpResponseHandler handler) throws JSONException {
        JSONObject json = new JSONObject();
        json.put("groupid", groupid);

        performPost(context, URL_BALANCE_GROUP, json, handler);
    }

    //***********************************GET ServicesLogin

    /**
     * Webservice returns JSON:
     * [
     * {"id":4865092394942464,"email":"timotest@gmail.com","name":"TimoTest"},
     * {"id":5331424576012288,"email":"timoest@gmail.com","name":"Timo"}, ...
     * ]
     *
     * @param filter
     * @param value
     * @param handler
     */
    protected static void getEntity(String url, String filter, String value, AsyncHttpResponseHandler handler){
        if (filter.equals(NO_FILTER)){
            performGet(url, handler);
        }
        else {
            RequestParams params = new RequestParams();
            params.add("filter", filter);
            params.add("value", value);
            performGet(url, params, handler);
        }
    }

    //***********************************DELETE Services

    protected static void deleteEntity(Context context, long id, String url, AsyncHttpResponseHandler handler){
        RequestParams params = new RequestParams();
        params.add("id", String.valueOf(id));
        performDelete(context, url, params, handler);
    }

    //***********************************DELETE Services

    /**
     * Webservice returns JSON:
     * [
     * {"id":5390255762964480,"name":"Malle 2015"},
     * {"id":5707733839904768,"name":"Testgrp 2015"},
     * ]
     *
     * @param handler
     */
    protected static void deleteGroup(Context context, AsyncHttpResponseHandler handler, long id){
        RequestParams params = new RequestParams();
        params.add("id",id+"");
        performDelete(context, URL_GROUP, params, handler);
    }




    //***********************************OTHER ServicesLogin
    public static void sendMail(String receiver, String title, String message, AsyncHttpResponseHandler handler){
        RequestParams params = new RequestParams();
        params.put("secret", "Zhs18k28");
        params.put("receiver", receiver);
        params.put("title", title);
        params.put("content", message);

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(URL_EMAIL, params, handler);
    }

    /* ----------------------------------------------------------------------------------------------------------------
     * Server communication
     * use those methods in the "Webservices" methods!
     */

    private static void performGet(String url, RequestParams params, AsyncHttpResponseHandler handler){
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, params, handler);
    }

    private static void performGet(String url, AsyncHttpResponseHandler handler){
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, handler);
    }

    private static void performPut(Context context, String url, JSONObject jsonContent, AsyncHttpResponseHandler handler){
        //create StringEntity of json that can be sent to server
        StringEntity se = null;
        try {
            se = new StringEntity(jsonContent.toString());

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

        //perform the put
        AsyncHttpClient client = new AsyncHttpClient();
        client.put(context, url, se, null, handler);
    }

    private static void performPost(Context context, String url, JSONObject jsonContent, AsyncHttpResponseHandler handler){
        //create StringEntity of json that can be sent to server
        StringEntity se = null;
        try {
            se = new StringEntity(jsonContent.toString());

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

        //perform the post
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(context, url, se, null, handler);
    }

    private static void performDelete(Context context, String url, RequestParams params, AsyncHttpResponseHandler handler){
        AsyncHttpClient client = new AsyncHttpClient();
        client.delete(context, url, null, params, handler);
    }
}
