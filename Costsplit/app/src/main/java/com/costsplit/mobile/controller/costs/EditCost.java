package com.costsplit.mobile.controller.costs;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.costsplit.mobile.controller.costs.overview.CostsActivity;
import com.costsplit.mobile.costsplit.R;
import com.costsplit.mobile.dbaccess.ServicesCosts;
import com.costsplit.mobile.model.CostItem;
import com.costsplit.mobile.model.Group;
import com.costsplit.mobile.model.User;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

public class EditCost extends ActionBarActivity {

    Button deleteCostButton;
    EditText costTitleField;
    EditText costAmountField;
    Spinner paidBySpinner;
    String paidBy;
    String[] userIdValues;
    ArrayAdapter<String> userOfGroupAdapter;
    private Toolbar toolbar;
    private int id_backbutton = 16908332;
    public static final int RESULT_EXPENSE_CHANGED = 121;
    public static final int RESULT_CANCEL = 122;
    private CostItem costitem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Toolbar einfaerben
        if (Build.VERSION.SDK_INT >= 21) {
            this.tintSystemBar();
        }

        setContentView(R.layout.activity_edit_cost);

        //Toolbar
        this.toolbar = (Toolbar)findViewById(R.id.edit_costs_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Edit Expense");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Wenn auf speichern geklickt wurde
        this.deleteCostButton = (Button) findViewById(R.id.edit_saveCostButton);
        this.deleteCostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteCostitem();
            }
        });
        this.costTitleField = (EditText)findViewById(R.id.edit_costTitleField);
        this.costAmountField = (EditText)findViewById(R.id.edit_costAmountField);

        final HashMap<Long, User> members = Group.myGroups.get(CostsActivity.groupid).getMembers();
        ArrayList<String> usernames = new ArrayList<>();
        this.userIdValues = new String[members.size()];
        int position = 0;
        for(User u: members.values()){
            usernames.add(u.getName());
            this.userIdValues[position] = String.valueOf(u.getId());
            position++;
        }

        paidBySpinner = (Spinner) findViewById(R.id.edit_paidBySpinner);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, usernames);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        paidBySpinner.setAdapter(arrayAdapter);

        //set values of selected item
        this.costitem = (CostItem) getIntent().getSerializableExtra("item");
        this.costTitleField.setText(this.costitem.getTitle());
        this.costAmountField.setText(this.costitem.getAmount()+"");

        for (int pos = 0; pos < this.userIdValues.length; pos++){
            Double id = Double.valueOf(this.userIdValues[pos]);
            if (id == this.costitem.getPayedById()){
                this.paidBySpinner.setSelection(pos);
                break;
            }
        }



        }

    private void deleteCostitem() {
        //delete old item
        AsyncHttpResponseHandler deleteHandler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                setResult(RESULT_EXPENSE_CHANGED);
                finish();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_LONG).show();
            }
        };

        ServicesCosts.deleteCostitem(getApplicationContext(), this.costitem.getId(), deleteHandler);
    }

    private void saveCostitem() {
        String costTitle = this.costTitleField.getText().toString();
        double amount = Double.valueOf(this.costAmountField.getText().toString());
        String useridStr = userIdValues[paidBySpinner.getSelectedItemPosition()];
        double userid = Double.valueOf(useridStr);

        //check if fields have valid fill
        if(costTitle.equals("")){
            Toast.makeText(getApplicationContext(), "What did you buy?", Toast.LENGTH_LONG).show();
            return;
        }
        if (costAmountField.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), "How much was it?", Toast.LENGTH_LONG).show();
            return;
        }

        //check if something was changed
        if((costTitle.equals(this.costitem.getTitle()))
                && (amount == this.costitem.getAmount())
                && (userid == this.costitem.getPayedById())){
            setResult(RESULT_CANCEL);
            finish();
        }
        else{
            //delete old item
            AsyncHttpResponseHandler deleteHandler = new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_LONG).show();
                }
            };

            ServicesCosts.deleteCostitem(getApplicationContext(), this.costitem.getId(), deleteHandler);

            //save new one
            AsyncHttpResponseHandler newCostHandler = new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    setResult(RESULT_EXPENSE_CHANGED);
                    finish();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_LONG).show();
                }
            };

            try {
                ServicesCosts.saveNewCost(getApplicationContext(), useridStr, String.valueOf(CostsActivity.groupid), amount, costTitle, newCostHandler);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_cost, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_done) {
            this.saveCostitem();
        }
        else if (id == this.id_backbutton){
            setResult(RESULT_CANCEL);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void tintSystemBar() {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        window.setStatusBarColor(getResources().getColor(R.color.blueDark03));
    }
}
