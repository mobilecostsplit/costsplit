package com.costsplit.mobile.dbaccess;

import android.content.Context;

import com.costsplit.mobile.model.CostItem;
import com.costsplit.mobile.model.Group;
import com.costsplit.mobile.model.User;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Timo on 30.03.15.
 * Use this class to access DBHelper-Methods.
 * This class acts as the interface to the controllers. Do not try to access DBHelper-Methods
 * from the controllers directly!
 */
public class ServicesCosts {


    /*
     * ----------------GET SERVICES----------------
     */
    public static void getCostItems(final long groupid, final ServicesCostsInterface delegate){
        final HashMap<Long, CostItem> list = new HashMap<>();

        AsyncHttpResponseHandler dataAvailHandler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = new String(responseBody);

                try {
                    JSONArray arr = new JSONArray(response);
                    final Group group = Group.myGroups.get(groupid);
                    if(group == null){System.err.println("Error: No data for Group with id +"+groupid);return;}
                    final HashMap<Long, User> members = group.getMembers();
                    if(members==null){System.err.println("Error: No members for Group with id +"+groupid); return;}

//                    for (Group g : Group.myGroups.values()){
//                        System.out.println(g);
//                        for (User u : g.getMembers().values()){
//                            System.out.println(u);
//                        }
//                    }

                    for (int i=0; i < arr.length(); i++){
                        JSONObject obj = arr.getJSONObject(i);
                        CostItem item = new CostItem();
                        item.setId(obj.getLong("id"));
                        item.setTitle(obj.getString("title"));
                        item.setAmount(obj.getDouble("value"));
                        item.setPayedByName(members.get(obj.getLong("userid")).getName());
                        item.setPayedById(obj.getLong("userid"));
                        list.put(item.getId(), item);
                    }

                    Group.myGroups.get(groupid).setCostitems(list);

                    delegate.onGroupDataAvailable(groupid);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                System.err.println("Error: Cost Items for groupid "+groupid+" could not be loaded!");
            }
        };

        DBHelper.getEntity(DBHelper.URL_COST_POSITION, DBHelper.FILTER_GROUPID, groupid+"", dataAvailHandler);
    }


    public static void getGroupById(final long groupid, final boolean shallLoadCostitems, final ServicesCostsInterface delegate){
        Group g = new Group();
        Group.myGroups.put(groupid, g);
        final Group group = Group.myGroups.get(groupid);

        final HashMap<Long, User> members = new HashMap<>();
        g.setMembers(members);
        final HashMap<Long, CostItem> costitems = new HashMap<>();
        g.setCostitems(costitems);

        AsyncHttpResponseHandler groupMetaHandler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = new String(responseBody);
                try {
                    JSONObject obj = new JSONObject(response);
                    group.setName(obj.getString("name"));
                    group.setId(obj.getLong("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                System.err.println("Error: Group meta data for id "+groupid+" could not be loaded ");
            }
        };

        final int[] memberProcess = {0,0}; // [0] = total number of members, [1] = current number of members

        final AsyncHttpResponseHandler userHandler = new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = new String(responseBody);
                try {
                    JSONObject obj = new JSONObject(response);
                    final User user = members.get(obj.getLong("id"));
                    user.setName(obj.getString("name"));
                    user.setEmail(obj.getString("email"));

                    memberProcess[1]++;
                    if (memberProcess[0] == memberProcess[1]){
                        if (shallLoadCostitems) {
                            getCostItems(groupid, delegate);
                        }
                        else{
                            delegate.onGroupDataAvailable(groupid);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                System.err.println("User data could not be loaded for group with id "+groupid);
            }
        };

        AsyncHttpResponseHandler groupMembersHandler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = new String(responseBody);
                try {
                    JSONArray arr = new JSONArray(response);
                    memberProcess[0] = arr.length();
                    for (int i=0; i < arr.length(); i++){
                        JSONObject obj = arr.getJSONObject(i);
                        User u = new User();
                        u.setId(obj.getLong("userid"));
                        members.put(u.getId(), u);

                        DBHelper.getEntity(DBHelper.URL_USER+"?id="+u.getId(), DBHelper.NO_FILTER, DBHelper.NO_FILTER, userHandler);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                System.err.println("Groupmembers could not be loaded for group with id "+groupid);
            }
        };

        DBHelper.getEntity(DBHelper.URL_GROUP+"?id="+groupid, DBHelper.NO_FILTER, DBHelper.NO_FILTER, groupMetaHandler);
        DBHelper.getEntity(DBHelper.URL_GROUP_ASSIGNMENT, DBHelper.FILTER_GROUPID, groupid+"", groupMembersHandler);
    }


    /*
     * ----------------POST SERVICES----------------
     */

    public static void clearGroup(Context context, long groupid, AsyncHttpResponseHandler handler) throws JSONException {
        DBHelper.clearGroup(context, groupid, handler);
    }

    /*
     * ----------------PUT SERVICES----------------
     */

    public static void saveNewCost(Context context,String userid, String groupId, double amount,String title,AsyncHttpResponseHandler handler) throws JSONException {
        DBHelper.addCostPosition(context, userid, groupId, amount, title, handler);
    }

    /*
     * ----------------DELETE SERVICES----------------
     */

    public static void deleteCostitem(Context context, long id, AsyncHttpResponseHandler handler){
        DBHelper.deleteEntity(context, id, DBHelper.URL_COST_POSITION, handler);
    }

    /*
     * ----------------OTHER SERVICES----------------
     */
}
