package com.costsplit.mobile.controller.costs.overview;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.costsplit.mobile.controller.ActivityConnections;
import com.costsplit.mobile.controller.costs.EditCost;
import com.costsplit.mobile.controller.costs.NewCost;
import com.costsplit.mobile.controller.group.EditGroupActivity;
import com.costsplit.mobile.controller.group.MyGroupsActivity;
import com.costsplit.mobile.costsplit.R;
import com.costsplit.mobile.dbaccess.ServicesCosts;
import com.costsplit.mobile.dbaccess.ServicesCostsInterface;
import com.costsplit.mobile.model.Group;
import com.costsplit.mobile.model.User;
import com.costsplit.mobile.util.SlidingTabLayout;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.software.shell.fab.ActionButton;

import org.apache.http.Header;
import org.json.JSONException;

// Testdaten
// groupid: 4814902145318912



// color palette
// http://www.google.com/design/spec/style/color.html#color-color-palette

// Making a toolbar
// http://www.android4devs.com/2015/01/how-to-make-material-design-sliding-tabs.html
// http://android-developers.blogspot.de/2014/10/appcompat-v21-material-design-for-pre.html
// http://www.101apps.co.za/index.php/articles/using-toolbars-in-your-apps.html
// http://blog.xamarin.com/android-tips-hello-toolbar-goodbye-action-bar/

// making a floating button
// http://www.google.de/imgres?imgurl=https%3A%2F%2Fwww.bignerdranch.com%2Fimg%2Fblog%2F2014%2F07%2Ffloating-action-button-2.gif&imgrefurl=https%3A%2F%2Fwww.bignerdranch.com%2Fblog%2Ffloating-action-buttons-in-android-l%2F&h=161&w=170&tbnid=39WA0eLBoM4cCM%3A&zoom=1&docid=I86qmm4WbJKK0M&ei=HBkcVeyvBYKfPNm_gKAH&tbm=isch&client=safari&iact=rc&uact=3&dur=619&page=5&start=95&ndsp=24&ved=0CA8QrQMwAzhk
// https://github.com/shell-software/fab

// use Snackbar instead of Toasts
// http://www.google.com/design/spec/components/snackbars-toasts.html#snackbars-toasts-specs

// use RecyclerView instead of Lists
// https://developer.android.com/training/material/lists-cards.html
// http://www.binpress.com/tutorial/android-l-recyclerview-and-cardview-tutorial/156

// back button in toolbar / actionbar
// http://stackoverflow.com/questions/14545139/android-back-button-in-the-title-bar

// swipe to refresh (myGroups-Screen, TabCosts-Screen)
// http://www.google.com/design/spec/patterns/swipe-to-refresh.html#swipe-to-refresh-swipe-to-refresh
// http://stackoverflow.com/questions/26807763/android-lollipop-pull-to-refresh
// http://antonioleiva.com/swiperefreshlayout/

// parent to child transition (choosing a group from the myGroups-Screen)
// http://www.google.com/design/spec/patterns/navigational-transitions.html#navigational-transitions-parent-to-child
// https://developer.android.com/training/material/animations.html#Transitions
// http://stackoverflow.com/questions/27235173/how-to-implement-the-parent-to-child-navigational-transition-as-prescribed-by

// Dividers in a recyclerview (horizontal lines between items)
// http://stackoverflow.com/questions/24618829/how-to-add-dividers-and-spaces-between-items-in-recyclerview

// Sections in a recyclerview (like subheadings)
// https://www.youtube.com/watch?v=PTnXzCCc4MA&spfreload=10

public class CostsActivity extends ActionBarActivity implements ServicesCostsInterface {

    ViewPager pager;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    Toolbar toolbar;
    CharSequence Titles[]={"Overview","Costs"};
    int Numboftabs =2;

    public static final int REQ_CODE_ADD_EXPENSE = 120;
    public static final int REQ_CODE_EDIT_EXPENSE = 220;
    public static final int REQ_CODE_EDIT_GROUP = 320;

    public static long groupid;
    private int id_backbutton = 16908332;
    private int response_code = MyGroupsActivity.RESP_CANCEL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= 21) {
            this.tintSystemBar();
        }

        setContentView(R.layout.activity_costs);

        //Toolbar
        this.toolbar = (Toolbar)findViewById(R.id.costs_tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("<Groupname>");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter =  new ViewPagerAdapter(getSupportFragmentManager(),Titles,Numboftabs);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);

        //Floating Action Button
        ActionButton b = (ActionButton)findViewById(R.id.costs_fab_button);
        b.setButtonColor(getResources().getColor(R.color.standard_orange));
        b.setButtonColorPressed(getResources().getColor(R.color.orangeDark02));
        b.setImageResource(R.drawable.fab_plus_icon);
        b.setShadowRadius(5.0f);
        b.setShadowYOffset(3.0f);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ActivityConnections.NEWCOST_PATH);
                startActivityForResult(i, REQ_CODE_ADD_EXPENSE);
            }
        });


        String groupidStr = getIntent().getStringExtra("groupid");
        groupid = Long.parseLong(groupidStr);

        getSupportActionBar().setTitle(Group.myGroups.get(groupid).getName());
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void tintSystemBar() {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        window.setStatusBarColor(getResources().getColor(R.color.blueDark03));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_costs, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            this.callGroupSettingsScreen();
        }
        else if (id == this.id_backbutton){
            setResult(this.response_code);
            finish();
        }
        else if (id == R.id.action_clear_balance){
            final CostsActivity This = this;

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked
                            yesPressed();
                            dialog.dismiss();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            dialog.dismiss();
                            break;
                    }
                }

                public void yesPressed(){
                    AsyncHttpResponseHandler handler = new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            refreshGroup();
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            System.err.println("Something went wrong!");
                        }
                    };

                    try {
                        ServicesCosts.clearGroup(This, groupid, handler);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure you want to delete all cost items? This cannot be reverted!").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void callGroupSettingsScreen() {
        Intent i = new Intent(getApplicationContext(), ActivityConnections.EDITGROUP_PATH);
        startActivityForResult(i, REQ_CODE_EDIT_GROUP);
    }

    protected void refreshGroup(){
        this.response_code = MyGroupsActivity.RESP_COSTS_GROUP;
        this.adapter.getTabCosts().getSwipeLayout().setRefreshing(true);
        this.adapter.getTabOverview().getSwipeLayout().setRefreshing(true);
        ServicesCosts.getGroupById(groupid, true, this);

    }

    @Override
    public void onGroupDataAvailable(long groupid) {
        this.adapter.getTabCosts().onGroupDataAvailable(groupid);
        this.adapter.getTabOverview().onGroupDataAvailable(groupid);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case (REQ_CODE_ADD_EXPENSE) : {
                if (resultCode == NewCost.RESULT_NEW_EXPENSE_ADDED) {
                    this.refreshGroup();
                }
                else if (resultCode == NewCost.RESULT_CANCEL){
                    // do nothing
                }
                break;
            }
            case (REQ_CODE_EDIT_EXPENSE) : {
                if (resultCode == EditCost.RESULT_EXPENSE_CHANGED) {
                    this.refreshGroup();
                }
                else if (resultCode == EditCost.RESULT_CANCEL){
                    // do nothing
                }
                break;
            }
            case (REQ_CODE_EDIT_GROUP) : {
                if (resultCode == EditGroupActivity.RESULT_CANCEL){
                    // do nothing
                }
                else{
                    this.refreshGroup();
                }
            }
        }
    }
}
