package com.costsplit.mobile.model.uielements;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Timo on 07.04.15.
 */
public class UICostOverview {

    private String name;
    private double amount;
    private int type;

    public UICostOverview(String name, double amount, int type){
        this.name = name;
        this.amount = Double.valueOf(new DecimalFormat("#.##").format(amount));
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = Double.valueOf(new DecimalFormat("#.##").format(amount));
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
