package com.costsplit.mobile.controller;

import com.costsplit.mobile.controller.costs.EditCost;
import com.costsplit.mobile.controller.costs.NewCost;
import com.costsplit.mobile.controller.costs.overview.CostsActivity;
import com.costsplit.mobile.controller.group.CreateNewGroupActivity;
import com.costsplit.mobile.controller.group.EditGroupActivity;
import com.costsplit.mobile.controller.group.MyGroupsActivity;
import com.costsplit.mobile.controller.login.PwForgottenActivity;
import com.costsplit.mobile.controller.login.registerActivity;

/**
 * Created by Timo on 27.03.15.
 * Use this class to set the different connections of your controller.
 * Read the static variables within your controller when switching screens.
 * See the example below.
 */
public class ActivityConnections {

    /*
     * --------- login and register screen
     */
    public static final Class REGISTER_PATH = registerActivity.class;
    public static final Class PWFORGOTTEN_PATH = PwForgottenActivity.class;
//    public static final Class LOGIN_WAY = <replace me>;



    /*
     * --------- group screens
     */

//    public static final Class CreateNewGroup_Path = CreateNewGroupActivity.class;

    public static final Class CreateNewGroup_Path = CreateNewGroupActivity.class;
    public static final Class EDITGROUP_PATH = EditGroupActivity.class;
    public static final Class ShowGroups_Path = MyGroupsActivity.class;


    /*
     * --------- costs screens
     */

    public static final Class COSTSACTIVITY_PATH = CostsActivity.class;
    public static final Class NEWCOST_PATH = NewCost.class;
    public static final Class EDITCOST_PATH = EditCost.class;
}
