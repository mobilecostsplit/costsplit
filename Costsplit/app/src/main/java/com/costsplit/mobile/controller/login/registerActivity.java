package com.costsplit.mobile.controller.login;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.costsplit.mobile.costsplit.R;
import com.costsplit.mobile.dbaccess.ServicesLogin;
import com.costsplit.mobile.util.Utility;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;

public class registerActivity extends Activity {

    private EditText nameTxt;
    private EditText emailAdrRegistrTxt;
    private EditText pwRegistrTxt;
    private EditText pwRegistrWdhTxt;
    private Button registrRegBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 21) {
            this.tintSystemBar();
        }
        setContentView(R.layout.activity_register);

        //get references of UI-elementsAmount
        this.nameTxt = (EditText) findViewById(R.id.nameTxt);
        this.emailAdrRegistrTxt = (EditText) findViewById(R.id.emailAdrRegistrTxt);
        this.pwRegistrTxt = (EditText) findViewById(R.id.pwRegistrTxt);
        this.pwRegistrWdhTxt = (EditText) findViewById(R.id.pwRegistrWdhTxt);
        this.registrRegBtn = (Button) findViewById(R.id.registrRegBtn);

        this.registrRegBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void tintSystemBar() {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        window.setStatusBarColor(getResources().getColor(R.color.blueDark03));
    }

    private void register() {
        final String name = this.nameTxt.getText().toString();
        final String email = this.emailAdrRegistrTxt.getText().toString();
        final String pw = this.pwRegistrTxt.getText().toString();
        final String pwWdh = this.pwRegistrWdhTxt.getText().toString();

        //validation
        if(!(Utility.isNotNull(name) && Utility.isNotNull(email) && Utility.isNotNull(pw) && Utility.isNotNull(pwWdh))){
            return;
        }
        if(!Utility.validate(email)){
            return;
        }
        if(!pw.equals(pwWdh)){
            return;
        }

        final AsyncHttpResponseHandler registerHandler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_LONG).show();
            }
        };

        final AsyncHttpResponseHandler userExistshandler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String resp = new String(responseBody);
                try {

                    JSONArray arr = new JSONArray(resp);
                    System.out.println(arr.toString());

                    if (arr.length() > 0){
                        //user does already exist
                        Toast.makeText(getApplicationContext(), "User does already exist!", Toast.LENGTH_LONG).show();
                    }
                    else{
                        ServicesLogin.registerUser(getApplicationContext(), name, email, pw, registerHandler);
                    }

                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_LONG).show();
            }
        };

        ServicesLogin.getUserByEmail(email, userExistshandler);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
