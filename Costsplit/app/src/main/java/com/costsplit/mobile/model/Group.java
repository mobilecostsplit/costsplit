package com.costsplit.mobile.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Timo on 02.04.15.
 */
public class Group {
    public static HashMap<Long, Group> myGroups = new HashMap<>();

    public static void addGroup(long id, String name, HashMap<Long, User> members){
        Group g = new Group();
        g.setId(id);
        g.setMembers(members);
        g.setName(name);
        myGroups.put(id, g);
    }



    private String name;
    private long id;
    private HashMap<Long, User> members = new HashMap<>();
    private HashMap<Long, CostItem> costitems = new HashMap<>();

    public HashMap<Long, CostItem> getCostitems() {
        return costitems;
    }

    public void setCostitems(HashMap<Long, CostItem> costitems) {
        this.costitems = costitems;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public HashMap<Long, User> getMembers() {
        return members;
    }

    public void setMembers(HashMap<Long, User> members) {
        this.members = members;
    }

    public String toString(){
        return "id: "+id+" name "+name;
    }
}
