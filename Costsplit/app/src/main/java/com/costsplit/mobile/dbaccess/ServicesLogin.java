package com.costsplit.mobile.dbaccess;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.costsplit.mobile.model.User;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Timo on 23.03.15.
 * Use this class to access DBHelper-Methods.
 * This class acts as the interface to the controllers. Do not try to access DBHelper-Methods
 * from the controllers directly!
 */
public class ServicesLogin {

    /*
     * ----------------GET SERVICES----------------
     */

    public static void getUserByEmail(String email, AsyncHttpResponseHandler handler){
        DBHelper.getEntity(DBHelper.URL_USER, DBHelper.FILTER_EMAIL, email, handler);
    }


    /*
     * ----------------POST SERVICES----------------
     */
    public static void loginUser(final Context context, final User loggedInUser, final String password, final AsyncHttpResponseHandler handler){
        AsyncHttpResponseHandler userByEmailHandler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = new String(responseBody);
                try {
                    JSONArray arr = new JSONArray(response);

                    if (arr.length() == 0){
                        Toast.makeText(context, "Wrong email or password entered! Try again.", Toast.LENGTH_LONG).show();
                        return;
                    }

                    JSONObject obj = arr.getJSONObject(0);
                    long id = obj.getLong("id");
                    loggedInUser.setId(id);
                    loggedInUser.setName(obj.getString("name"));
                    DBHelper.loginUser(context, id, password, handler);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        };

        getUserByEmail(loggedInUser.getEmail(), userByEmailHandler);
    }

    /*
     * ----------------PUT SERVICES----------------
     */
    public static void registerUser(Context context, String name, String email, String password, AsyncHttpResponseHandler handler){
        try {
            DBHelper.addUser(context, name, email, password, handler);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /*
     * ----------------DELETE SERVICES----------------
     */



    /*
     * ----------------OTHER SERVICES----------------
     */

    public static void pwForgottenEmail(final Context context, final String emailAdr, final AsyncHttpResponseHandler handler){
        //retrieve userid for email.
        AsyncHttpResponseHandler handlerUseridForEmail = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    JSONArray jsonArr = new JSONArray(new String(responseBody));

                    if (jsonArr.length() == 0){
                        Toast.makeText(context, "There is no user with the email provided.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    JSONObject json = jsonArr.getJSONObject(0);
                    long id = json.getLong("id");


                    //retrieve password for userid
                    AsyncHttpResponseHandler handlerPasswordForUserid = new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            try {
                                System.out.println(new String(responseBody));
                                JSONObject json = new JSONObject(new String(responseBody));
                                String pw = json.getString("password");

                                //send email with password to emailAdr.
                                DBHelper.sendMail(emailAdr, "Your CostSplit Password", "Your CostSplit password: "+pw, handler);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            error.printStackTrace();
                        }
                    };

                    String url = DBHelper.URL_USER+"?id="+id;
                    DBHelper.getEntity(url, DBHelper.NO_FILTER, null, handlerPasswordForUserid);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
            }
        };

        DBHelper.getEntity(DBHelper.URL_USER, DBHelper.FILTER_EMAIL, emailAdr, handlerUseridForEmail);
    }

}
